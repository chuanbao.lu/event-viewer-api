# Event Viewer Back End API

This application is a Spring Boot REST API application. It uses io.fabric8 docker-maven-plugin to build, run, stop docker image. It loads the test data json file into h2 memory db when the server starts up.

## Getting Started

### Prerequisites

* Maven 3.6.0 or newer version
* JDK 1.8 or newer version
* Docker 19.03

### Buiding

Create a folder in you local file system, 

```
mkdir project
cd project
```

Create a data folder

```
mkdir data
```
Copy the "assignment_data_full.json" to data folder.

Git clone this project to project folder.

```
git clone git@gitlab.com:chuanbao.lu/event-viewer-api.git
```

Enter event-viewer-api folder.

```
cd event-viewer-api
```

The file structure should look like this:

```
project
    |
    - data
    |   |
    |   - assignment_data_full.json
    |
    - event-viewer-api
        |
        - All project files are here
```

Build the application, docker image.
	
```
mvn clean install
```

### Run the app

Now a docker image "events-api" is created.  You can run it with regular docker command with port mapping 8080:8080 and volume bind <the path of data folder>:/var/data. Here is an example of it.

```
docker run --rm -d --name events-api -p 8080:8080 -v /project/data:/var/data events-api
```

The other way to run docker is to use Maven docker plugin, which is already setup properly in this project.

Run docker image with log output, use ctl+c to stop it. 

```
mvn docker:run
```
Run docker image without log output.

```
mvn docker:start
```
Stop docker image when you want to stop the server.

```
mvn docker:stop
```

Remove docker image and container when you want to clean up docker image and container.

```
mvn docker:remove
```
	
### Test the application

* Follow above the installation to start the application in a docker container. 
* Access http://localhost:8080/ to check if you see "Server is running" in your browser. 

## End Points: 

### Search events

The search end point return events information without event_details.

Parameter:

1. start: search events greater than or equal to start date

2. end: search events less than end date
   
3. If no start or end value presents, then it means search all.
   
For example: 

```
# Search events greater than 20100131 and less than 20190131
GET http://localhost:8080/api/event?start=20100131&end=20190131

# Search events greater than 20100131
GET http://localhost:8080/api/event?start=20100131

# Search events less than 20190131
GET http://localhost:8080/api/event?end=20190131

# Search all events
GET http://localhost:8080/api/event
```

### Get Event Detail

This end point return full information of a event. 

Parameter:

1. id: search a event, which ID equals id
   
For example: 

```
# Search event which ID is 10
http://localhost:8080/api/10
```

### Get Earliest Event Date

When the applciation startup, set the default start date to be the earliest event date.

```
GET http://localhost:8080/api/earliest-event-date
```

## Authors

**Chuanbao Lu** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
