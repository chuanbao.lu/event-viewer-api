package com.example.data;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

	@Query("select new com.example.model.Event(e.id, e.eventDate, e.eventType, e.eventSummary, e.eventSize) "
			+ "from Event e where e.eventDate >= :start and e.eventDate < :end order by e.eventDate")
	Iterable<Event> findByDateRage(@Param("start") Date startDate, @Param("end") Date endDate);

	@Query("select new com.example.model.Event(e.id, e.eventDate, e.eventType, e.eventSummary, e.eventSize) "
			+ "from Event e order by e.eventDate")
	Iterable<Event> findAllEvent();

	@Query("select new com.example.model.Event(e.id, e.eventDate, e.eventType, e.eventSummary, e.eventSize) "
			+ "from Event e where e.eventDate >= :start order by e.eventDate")
	Iterable<Event> findGreaterThanEqualStartDate(@Param("start") Date startDate);

	@Query("select new com.example.model.Event(e.id, e.eventDate, e.eventType, e.eventSummary, e.eventSize) "
			+ "from Event e where e.eventDate < :end order by e.eventDate")
	Iterable<Event> findLessThanEndDate(@Param("end")Date endDate);

	@Query("select min(eventDate) from Event")
	Optional<Date> findEarlistEventDate();
}
