package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.service.EventService;

@Controller
public class HomeController {
	
	@Autowired
	private EventService eventService;
	
	@ResponseBody
	@RequestMapping( path = "/")
	public String goHome() {
		return "Server is running. " + eventService.getDataLoadingStatus();
	}

	public EventService getEventService() {
		return eventService;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

}
