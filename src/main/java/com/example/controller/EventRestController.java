package com.example.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Event;
import com.example.model.EventSearchResult;
import com.example.service.EventService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/api", produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
public class EventRestController {

	@Autowired
	private EventService eventService;

	@RequestMapping(path = "/event", method = RequestMethod.GET)
	public ResponseEntity<EventSearchResult> findEvents(
			@RequestParam(name = "start", required = false) @DateTimeFormat(pattern = "yyyyMMdd") Date start,
			@RequestParam(name = "end", required = false) @DateTimeFormat(pattern = "yyyyMMdd") Date end) {
		return new ResponseEntity<EventSearchResult>(new EventSearchResult(start, end, eventService.findEvents(start, end)),
				HttpStatus.OK);
	}

	@RequestMapping(path = "/event/{id}", method = RequestMethod.GET)
	public ResponseEntity<Event> findEventDetails(@PathVariable("id") Long id) {
		return this.eventService.findEventDetail(id).map(event -> ResponseEntity.ok().body(event)) // 200 OK
				.orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@RequestMapping(path = "/earliest-event-date", method = RequestMethod.GET)
	public ResponseEntity<String> findEarlistEventDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return this.eventService.findEarlistEventDate().map(date -> ResponseEntity.ok().body(sdf.format(date))) // 200 OK
				.orElseGet(() -> ResponseEntity.notFound().build());
	}

	public EventService getEventService() {
		return eventService;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

}
