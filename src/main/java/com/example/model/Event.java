package com.example.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "event_date")
	@JsonProperty("event_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
	private Date eventDate;

	@Column(name = "event_type")
	@JsonProperty("event_type")
	private String eventType;

	@Column(name = "event_summary")
	@JsonProperty("event_summary")
	private String eventSummary;

	@Column(name = "event_size")
	@JsonProperty("event_size")
	private Long eventSize;

	@Column(name = "event_details", columnDefinition = "CLOB")
	@JsonProperty("event_details")
	private String eventDetails;

	public Event() {

	}

	public Event(Long id, Date eventDate, String eventType, String eventSummary, Long eventSize) {
		this.id = id;
		this.eventDate = eventDate;
		this.eventType = eventType;
		this.eventSummary = eventSummary;
		this.eventSize = eventSize;
	}
	
	public Event(Long id, Date eventDate, String eventType, String eventSummary, Long eventSize, String details) {
		this(id, eventDate,eventType, eventSummary, eventSize);
		this.eventDetails = details;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventSummary() {
		return eventSummary;
	}

	public void setEventSummary(String eventSummary) {
		this.eventSummary = eventSummary;
	}

	public Long getEventSize() {
		return eventSize;
	}

	public void setEventSize(Long eventSize) {
		this.eventSize = eventSize;
	}

	public String getEventDetails() {
		return eventDetails;
	}

	public void setEventDetails(String eventDetails) {
		this.eventDetails = eventDetails;
	}

}
