package com.example.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.data.EventRepository;
import com.example.model.Event;

@Service
public class EventService {
	
	@Autowired
	private EventRepository eventRepository;
	
	private String dataLoadingStatus;

	public void save(List<Event> events) {
		eventRepository.saveAll(events);		
	}
	
	public Iterable<Event> findEvents(Date startDate, Date endDate) {
		if (startDate != null && endDate!= null) {
			return this.eventRepository.findByDateRage(startDate, endDate);
		} else if (startDate == null && endDate == null){
			return this.eventRepository.findAll();
		} else {
			if (startDate != null) {
				return this.eventRepository.findGreaterThanEqualStartDate(startDate);
			} else {
				return this.eventRepository.findLessThanEndDate(endDate);
			}
		}
	}
	
	public Optional<Event> findEventDetail(Long id) {
		return this.eventRepository.findById(id);
	}

	public Optional<Date> findEarlistEventDate() {
		return this.eventRepository.findEarlistEventDate();
	}

	public EventRepository getEventRepository() {
		return eventRepository;
	}

	public void setEventRepository(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	public String getDataLoadingStatus() {
		return dataLoadingStatus;
	}

	public void setDataLoadingStatus(String dataLoadingStatus) {
		this.dataLoadingStatus = dataLoadingStatus;
	}
	
}
