package com.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.model.Event;
import com.example.service.EventService;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class EventsApiApplication {
	
	private final static Logger logger = LoggerFactory.getLogger(EventsApiApplication.class);
	
	@Value("${data.path}")
	private String dataPath;
	
	@Autowired
	private EventService eventService;

	public static void main(String[] args) {
		SpringApplication.run(EventsApiApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(EventService service) {
		return args -> {
			ObjectMapper mapper = new ObjectMapper();
			// The JSON file contains change line characters, the next line to allow the parser to deal with this
			mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
			TypeReference<List<Event>> typeRefrence = new TypeReference<List<Event>>() {};
			try {
				InputStream is = new FileInputStream(new File(dataPath));
				List<Event> events = mapper.readValue(is, typeRefrence);
				service.save(events);
				eventService.setDataLoadingStatus(events.size() + " events were loaded.");
				logger.info("Events data loaded!");
			} catch (IOException e) {
				logger.error("Unable to load events data.", e);
				eventService.setDataLoadingStatus("Unable to load events data. Error: " + e.getMessage());
			}
		};
	}

}
