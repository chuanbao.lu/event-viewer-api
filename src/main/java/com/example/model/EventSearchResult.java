package com.example.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class EventSearchResult {
	private Iterable<Event> events;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
	private Date startDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
	private Date endDate;
	
	public EventSearchResult() {
		
	}
	
	public EventSearchResult(Date startDate, Date endDate, Iterable<Event> events) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.events = events;
	}

	public Iterable<Event> getEvents() {
		return events;
	}

	public void setEvents(Iterable<Event> events) {
		this.events = events;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
